import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers import skyviews

# https://fastapi.tiangolo.com/tutorial/sql-databases/

app = FastAPI(
    title="ADEX backend",
    description="ADEX backend FastAPI",
    version="1.0.0",
    docs_url='/adex-fastapi/docs',
    redoc_url='/adex-fastapi/redoc',
    openapi_url='/adex-fastapi/openapi.json',
    contact={
        "name": "Nico Vermaas",
        "email": "vermaas@astron.nl",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },)

app.include_router(skyviews.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)