# ADEX-BACKEND-FASTAPI

## Development/Test

  * Documentation : https://support.astron.nl/confluence/display/SDCP/ADEX+backend+exploration
  * Deployment Diagram: https://app.diagrams.net/#G10-LtvKbhC-yzjVoTIg1bnr1wBKsA4s-V


### localhost (laptop)
  * http://localhost:8000/adex-fastapi/skyviews/
  * http://localhost:8000/adex-fastapi/skyviews_rectangle/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000
  * http://localhost:8000/adex-fastapi/skyviews_dataproducts/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000&collections=apertif-imaging&levels=2

#### filters  
  * http://localhost:8000/adex-fastapi/skyviews_dataproducts/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000&collections=apertif-imaging&levels=2
  
#### API documentation  
  * http://localhost:8000/adex-fastapi/docs
  * http://localhost:8000/adex-fastapi/redoc
  
### sdc-dev (test environment)  
  * https://sdc-dev.astron.nl/adex-fastapi/skyviews/
  * https://sdc-dev.astron.nl/adex-fastapi/skyviews_rectangle/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000
  
### SURF Research Cloud (dev environment)
  * frontend app for testing: http://145.38.187.31/aladin-testbed/

  * http://145.38.187.31/adex-fastapi/skyviews/
  * http://145.38.187.31/adex-fastapi/skyviews_rectangle/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000
  
#### filters  
  * http://145.38.187.31/adex-fastapi/skyviews_dataproducts/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000&collections=apertif-imaging&levels=2
    
## data procedures (rough)
  * [dump/load a copy of the alta database](https://web-of-wyrd.nl/myworkwiki/doku.php?id=alta_runtime&s[]=dump#qhow_do_i_dump_and_load_a_copy_of_the_production_database_with_postgres_into_a_dev_or_vm_environment)
  * [dump/load esap_cache sql](https://web-of-wyrd.nl/myworkwiki/doku.php?id=ucac4_converter&s[]=ucac4#performance)  
  
  
### load esap_cache.sql into dockerized Postgres database
manually copy `esap_cache.sql` to ~/shared/sql directory 
manually drop the skyviews table

then load the data like this:

On sdc-dev:
```
docker exec -it adex-postgres psql -U postgres -d adex_cache -c "DROP TABLE skyviews"
docker exec -it adex-postgres psql -U postgres -d adex_cache -f /shared/sql/adex_cache.sql
```
Test: https://sdc-dev.astron.nl/adex-fastapi/skyviews/

On SURF:
```
sudo docker exec -it Postgres14 psql -U postgres -d adex_cache -c "DROP TABLE skyviews"
sudo docker exec -it Postgres14 psql -U postgres -d adex_cache -f /shared/adex_cache.sql
```
Test: http://145.38.187.31/adex-fastapi/skyviews/
