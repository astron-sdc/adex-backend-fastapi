from typing import List, Union

from fastapi import APIRouter, Query, Depends
from sqlalchemy.orm import Session
from database import crud, models, schemas
from database.database import SessionLocal, engine

router = APIRouter(prefix="/adex-fastapi", tags=["skyviews"],)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def get_all_dp_types():
    return ['image','cube','timeSeries','visibility']

def get_all_dp_subtypes():
    return ['uncalibratedVisibility','lineCube','continuumCube','beamCube','continuumChunk','polarizationCube',
            'pulsarTimingTimeSeries','compressedUncalibratedVisibility','splitttedUncalibratedVisibility',
            'calibratedImage','calibratedVisibility','continuumMF','polarisationImage']

# http://127.0.0.1:8000/skyviews/
# http://127.0.0.1:8000/skyviews/?skip=100&limit=100
@router.get("/skyviews/", tags=["skyviews"], response_model=List[schemas.SkyView])
async def get_skyviews(skip: int = 0, limit: int = 1000, db: Session = Depends(get_db)):
    items = crud.get_skyviews(db, skip=skip, limit=limit)
    return items

# http://localhost:8000/adex-fastapi/skyviews_rectangle/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000
@router.get("/skyviews_rectangle/", tags=["skyviews"], response_model=List[schemas.SkyView])
async def get_skyviews_rectangle(db: Session = Depends(get_db),
                                 ra_min: float = 0.0, ra_max: float = 1.0, dec_min: float = 0.0, dec_max: float = 1.0,
                                 limit: int = 1000):
    items = crud.get_skyviews_rectangle(db, ra_min=ra_min, ra_max=ra_max, dec_min=dec_min, dec_max=dec_max, limit=limit)
    return items

# http://localhost:8000/adex-fastapi/skyviews_dataproducts/?ra_min=40&ra_max=50&dec_min=25&dec_max=35&limit=1000&dp_types=image,ube
@router.get("/skyviews_dataproducts/", tags=["skyviews"], response_model=List[schemas.SkyView])
async def get_skyviews_dataproducts(db: Session = Depends(get_db),
                                    ra_min: float = 0.0, ra_max: float = 1.0, dec_min: float = 0.0, dec_max: float = 1.0,
                                    collections: str = None,
                                    beams: str = None,
                                    levels : str = None,
                                    dp_types: str = None,
                                    dp_subtypes: str = None,
                                    limit: int = 1000):

    # TODO: get rid of these hardcoded values
    # no values should result in something like 'all',
    # but I don't know how to do that in SQLAlchemy query filters

    try:
        collections_list = collections.split(',')
    except:
        collections_list = ['apertif-imaging','apertif-timeseries']

    try:
        beams_list = beams.split(',')
    except:
        beams_list = [0]

    try:
        levels_list = levels.split(',')
    except:
        levels_list = [0,1,2]

    try:
        dp_types_list = dp_types.split(',')
        if 'all' in dp_types_list:
            dp_types_list = get_all_dp_types()
    except:
        # if no dp_types are given, assume 'image' (better than nothing)
        dp_types_list = get_all_dp_types()

    try:
        dp_subtypes_list = dp_subtypes.split(',')
        if 'all' in dp_subtypes_list:
            dp_subtypes_list = get_all_dp_subtypes()
    except:
        # if no dp_types are given, assume 'image' (better than nothing)
        dp_subtypes_list = get_all_dp_subtypes()

    items = crud.get_skyviews_dataproducts(
        db, ra_min=ra_min, ra_max=ra_max, dec_min=dec_min, dec_max=dec_max,
        collections=collections_list,
        beams=beams_list,
        levels=levels_list,
        dp_types=dp_types_list,
        dp_subtypes=dp_subtypes_list,
        limit=limit)

    return items
