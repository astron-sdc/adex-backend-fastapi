from sqlalchemy import Column, Integer, String, Float, DateTime

from .database import Base

# sql alchemy model
class SkyView(Base):
    #__tablename__ = "skyviews"             # using the adex_cache_fastapi.skyviews table
    __tablename__ = "adex_cache_skyview"    # using the adex_cache_django.adex_cache_skyview table
    id = Column(Integer, primary_key=True, index=True)
    pid = Column(String)
    name = Column(String)
    ra = Column(Float, index=True)
    dec = Column(Float, index=True)
    observation = Column(String)
    beam = Column(Integer)
    collection = Column(String)
    level = Column(Integer)
    dataproduct_type = Column(String)
    dataproduct_subtype = Column(String)
    access_url = Column(String)