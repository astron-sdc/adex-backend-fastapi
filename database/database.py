import os
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# dev machine on SURFSara
# DATABASE_URL = "postgresql://postgres:secret@145.38.187.31/adex_cache"

# read from environment
DATABASE_URL = os.environ.get('DATABASE_URL', 'postgresql://postgres:postgres@localhost/adex_cache_fastapi')
print('DATABASE_URL = '+DATABASE_URL)

engine = create_engine(
    DATABASE_URL, pool_size=3, max_overflow=0
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

