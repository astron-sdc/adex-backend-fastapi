from sqlalchemy.orm import Session

from .models import SkyView
from utils.utils import timeit

@timeit
def get_skyviews(db: Session, skip: int = 0, limit: int = 1000):
    return db.query(SkyView).offset(skip).limit(limit).all()

@timeit
def get_skyviews_rectangle(db: Session, ra_min: float = 0.0, ra_max: float = 1.0, dec_min: float = 0.0, dec_max: float = 1.0, limit: int = 1000):
    list = db.query(SkyView).filter(
        SkyView.ra > ra_min,
        SkyView.ra < ra_max,
        SkyView.dec > dec_min,
        SkyView.dec < dec_max,
    ).limit(limit).all()
    print("retrieved "+str(len(list)) + ' dataproducts')
    return list

@timeit
def get_skyviews_dataproducts(
        db: Session,
        ra_min: float = 0.0, ra_max: float = 1.0,
        dec_min: float = 0.0, dec_max: float = 1.0,
        limit: int = 1000,
        collections: list = None,
        beams : list = None,
        levels : list = None,
        dp_types : list = None,
        dp_subtypes: list = None):

    # https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_using_query.htm
    # https://docs.sqlalchemy.org/en/14/orm/query.html
    list = db.query(SkyView).filter(
        SkyView.ra > ra_min,
        SkyView.ra < ra_max,
        SkyView.dec > dec_min,
        SkyView.dec < dec_max,
        SkyView.collection.in_(collections),
        #SkyView.beam.in_(beams),
        SkyView.level.in_(levels),
        SkyView.dataproduct_type.in_(dp_types),
        SkyView.dataproduct_subtype.in_(dp_subtypes),
    ).limit(limit).all()
    print("retrieved "+str(len(list)) + ' dataproducts')
    return list