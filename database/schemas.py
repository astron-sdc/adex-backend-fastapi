
from pydantic import BaseModel
from typing import Optional

# Pydantic models help you define request payload models
# and response models in Python Class object notation

class SkyView(BaseModel):
    id: int
    pid: str
    name: str
    ra: float
    dec: float
    observation: str
    beam: int
    collection: str
    level: int
    dataproduct_type: str
    dataproduct_subtype: str
    access_url: str

    class Config:
        orm_mode = True